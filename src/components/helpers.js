export const toNumber = (value) => {
    const number = +value;
    return !number || isNaN(number) ? value : number;
};
