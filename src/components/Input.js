import React, { PureComponent } from 'react';

class Input extends PureComponent {
    render() {
        const { props } = this;
        return (
            <input
                type="text"
                name={props.name}
                value={props.value}
                id={props.id || props.name}
                onChange={props.onChange}
            />)
    }
}
export default Input;
