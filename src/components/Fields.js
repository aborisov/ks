import React from 'react';

import InputComponent from './Input.js';

export const Input = ({
    type = 'text',
    label,
    name,
    input,
    id,
    intent,
    ...props
}) => (
    <div intent={intent}>
        <label htmlFor={id || name}>{label}</label>
        <InputComponent
            type={type}
            name={name}
            id={name}
            {...input}
            {...props}
        />
    </div>
);
