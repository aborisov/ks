
function throttledPromise(promise, ...args) {
    return async function fetchThrottledJsonp(items, concurency = 1, resultCallback) {
        let tasksProcessing = 0;
        let tasksPaused = [];
        let error;
        return Promise.all(items.map(item => {
            return new Promise(async (resolve, reject) => {
                if (tasksProcessing > concurency - 1) {
                    await new Promise(resolve => tasksPaused.push(resolve));
                }
                if (error) {
                    return;
                }
                tasksProcessing++;
                return promise(item, ...args)
                    .then(response => {
                        tasksProcessing--;
                        tasksPaused.length && tasksPaused.shift()()
                        resolve(resultCallback(response, tasksPaused.length));
                        //resolve(response);
                    }).catch(err => {
                        error = true;
                        reject(err);
                    });
            });
        }));
    }
}

export default throttledPromise;
