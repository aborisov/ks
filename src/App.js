import React, { Component, Fragment } from 'react';
import './App.css';
import { Input } from './components/Fields';

import fetchJsonp from 'fetch-jsonp';
import throttledPromise from './utils/throttledPromise.js';

const URL = 'http://localhost:9999/mock?from=';


class App extends Component {

    constructor() {
        super()
        this.currentYear = (new Date()).getFullYear();
        this.state = { from: '', to: '' };

        this.validate = this.validate.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onSubmit(e) {
        this.validate();

        const { state, currentYear } = this;
        const from = state.from || 2010;
        const to = state.to || currentYear;

        this.setState({
            loading: true,
            error: null,
            result: '',
            loaderProgress: 0,
            disabled: true,
            from,
            to,
        });

        let urls = [];
        for (var i = from; i <= to; i++) {
            urls.push(`${URL}${i}`);
        }

        const perResultCallback = (response, count) => {
            const loaderProgress = (((urls.length - count) / urls.length) * 100).toFixed();
            this.setState({ loaderProgress });
            return response.json();
        };

        throttledPromise(fetchJsonp, {
            timeout: 2000,
        })(urls, 2, perResultCallback)
            .then(res => {
                const KEYS = ["l", "h", "s", "e"];
                const resArray = res.reduce((memo, item) => memo.concat(item.ohlc), []);
                const result = KEYS.map(key => ({ key, max: getMax(resArray, key), min: getMin(resArray, key) }));

                this.setState({
                    loading: false,
                    error: false,
                    result,
                    disabled: false,
                });
            })
            .catch(error => {
                this.setState({
                    loading: false,
                    error: error.toString(),
                    disabled: false,
                });
            });

        e.preventDefault();

        function getMin(data, key) {
            return data.reduce((min, p) => p[key] < min ? p[key] : min, data[0][key]);
        }

        function getMax(data, key) {
            return data.reduce((max, p) => p[key] > max ? p[key] : max, data[0][key]);
        }
    }

    onChange(e) {
        const { state } = this;
        const { name, value } = e.target;
        const obj = {};
        obj[name] = value;

        this.setState(obj, () => this.validate());
    }

    validate() {
        const { state, currentYear } = this;
        let { from, to } = state;
        from = from || 2010;
        to = to || currentYear;

        const validatorFrom = /^\d*$/.test(from)
            && (from === "" || (parseInt(from, 10) <= currentYear && parseInt(from, 10) >= 2010 && parseInt(from, 10) <= parseInt(to, 10)));
        const validatorTo = /^\d*$/.test(to) && (to === "" || (parseInt(to, 10) <= currentYear && parseInt(to, 10) >= 2010 && parseInt(to, 10) >= parseInt(from, 10)));

        this.setState({ disabled: !(validatorFrom && validatorTo) });
    }

    render () { 
        return(
            <div className="App">
                <header className="App-header">
                <form onSubmit={this.onSubmit}>
                    <Input
                        label="Year from"
                        name="from"
                        value={this.state.from}
                        onChange={this.onChange}
                        intent={this.disableSubmit}
                    />
                    <Input
                        label="Year to"
                        name="to"
                        value={this.state.to}
                        onChange={this.onChange}
                        intent={this.disableSubmit}
                    />
                    <input
                        type="submit"
                        value="Submit"
                        disabled={this.state.disabled ? 'disabled' : ''}
                    />
                </form>
                <Fragment>
                    {this.state.error ?
                        (<p>{this.state.error}</p>)
                        : <Fragment>
                            {this.state.loading && <p>Loading {this.state.loaderProgress}%...</p>}
                            {this.state.result && <ul>
                                    {this.state.result.map((item) => 
                                        <li key={item.key}>
                                          {item.key}: min: {item.min} max: {item.max}
                                        </li>
                                    )}
                                </ul>
                            }
                        </Fragment>
                    }
                </Fragment>
                </header>
            </div>
        );
    }
}

export default App;
