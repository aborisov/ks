'use strict';

const http = require('http');
const url  = require('url');
const fs   = require('fs');
const port = process.env.PORT || 9999;

fs.readFileSync('index.js');

function candlesMock() {
    const KEYS = ["l", "h", "s", "e"];

    return [...Array(Math.floor(Math.random() * 101)).keys()].map((item) => {
        return KEYS.reduce((memo, key) => {
            memo[key] = (Math.random() + 1).toFixed(8);
            return memo;
        }, {});
    });
}

function notFound(req, res) {
    res.statusCode = 404;
    res.setHeader('Content-Type', 'text/plain');
    res.end('Not found');
}

function requestError(req, res) {
    res.statusCode = 400;
    res.setHeader('Content-Type', 'text/plain');
    res.end('Bad request');
}

function jsonpResponse(req, res, data) {
    const { query } = url.parse(req.url, true);
    const { callback } = query;
    if (!callback) {
        return requestError(req, res);
    }
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/javascript');
    res.end(`${callback}(${JSON.stringify(data)});`);
}

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

const requestHandler = async (req, res) => {
    const { pathname, query } = url.parse(req.url, true);
    if (req.method !== 'GET') {
        return notFound(req, res);
    } else if (pathname === "/mock") {
        if (Math.random() < 0.05) {
            return notFound(req, res);
        }
        const { callback, year } = query;
        const ohlc = candlesMock();
        await sleep(Math.random() * 2000);
        return jsonpResponse(req, res, { status: 200, ohlc });
    }
    return notFound(req, res);
}
const server = http.createServer(requestHandler)
server.listen(port, (err) => {
    if (err) {
        return console.log('Something went wrong', err);
    }
    console.log(`Server is listening on ${port}`)
})

